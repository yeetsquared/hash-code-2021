module MyLib where

import qualified Data.Set as DS
import Data.Set (Set, fromList, powerSet, elems)
import Data.List
import Data.Maybe
import Data.Foldable (foldl')
import Control.Monad
import Text.Pretty.Simple (pPrint)

----- Data definition stuff -----

data Pizza = Pizza { indexOfPizza :: Integer
                   , numberOfIngredients :: Integer
                   , ingredientsOfPizza :: Set String
                   } deriving (Show, Eq, Ord)

data Teams = Teams { teamsOf2 :: Integer
                   , teamsOf3 :: Integer
                   , teamsOf4 :: Integer
                   } deriving (Show, Eq, Ord)

data In = In { numberOfPizzas :: Integer
             , teamsIn :: Teams
             , pizzasIn :: Set Pizza
             } deriving (Show, Eq, Ord)

enumerate :: [a] -> [(Integer, a)]
enumerate = zip [0..]

cutHeadTail :: [a] -> (a, [a])
cutHeadTail = fromMaybe (error "sepHead: got literally Nothing")
            . uncons

parsePizza :: Integer -> String -> Pizza
parsePizza n s = Pizza n (fromIntegral $ length ings) ings
  where ings = fromList $ tail $ words s

parseNOfPizzasAndTeams :: String -> (Integer, Teams)
parseNOfPizzasAndTeams = doubleParse . cutHeadTail . words
  where listToTeams [a, b, c] = Teams a b c
        listToTeams _ = error "Why doesn't the list have 3 arguments?"
        doubleParse (t, p) = (read t, listToTeams $ read <$> p)

parseInput :: String -> In
parseInput = parsed . cutHeadTail . lines
  where parsed (t, p) = In nOfPizzas teamsIn' pizzasIn'
          where (nOfPizzas, teamsIn') = parseNOfPizzasAndTeams t
                pizzasIn' = fromList $ uncurry parsePizza <$> enumerate p

----- Processing stuff -----

totalIngredients :: Set Pizza -> Integer
totalIngredients = sum . DS.map numberOfIngredients

-- Examples:
-- (Teams 1 2 1) ==> [4, 3, 3, 2]
-- (Teams 2 5 3) ==> [4, 4, 4, 3, 3, 3, 3, 3, 2, 2]
-- Teams of 4 first to maximize score.
teamsToList :: Teams -> [Integer]
teamsToList (Teams a b c) = listOf c 4 <> listOf b 3 <> listOf a 2
  where listOf a b = take (fromIntegral a) $ repeat b

teamsForPizzas :: In -> [Integer]
teamsForPizzas input = loopY (teamsToList $ teamsIn input)
  where n = numberOfPizzas input
        fuck = error "no adequate teambag found"
        loopY :: [Integer] -> [Integer]
        loopY []       = fuck
        loopY (y : ys) = case temp of
                           Nothing -> loopY ys
                           Just xs -> xs
          where loopX :: [Integer] -> [Integer] -> Integer -> Maybe [Integer]
                loopX []       accs sa = fuck
                loopX (x : xs) accs sa
                  | sa == n   = Just accs
                  | sa < n    = loopX xs (x : accs) (sa + x)
                  | otherwise = fuck
                
                temp = loopX (y : ys) [] 0

----- IO stuff -----

getAContent = readFile "../examples/a_example"
getBContent = readFile "../examples/b_little_bit_of_everything.in"
getCContent = readFile "../examples/c_many_ingredients.in"
getDContent = readFile "../examples/d_many_pizzas.in"
getEContent = readFile "../examples/e_many_teams.in"

testTeamMembers :: String -> IO ()
testTeamMembers content = do
    let parsedContent = parseInput content
    putStr "Teams: "
    pPrint $ teamsIn parsedContent
    putStr "Teams for pizzas: "
    print $ teamsForPizzas parsedContent

main :: IO ()
main = do
    content <- getAContent
    let parsedContent = parseInput content
    putStrLn "Parsed content:"
    pPrint parsedContent
    let (In nOfPizzas teams pizzas) = parsedContent
    putStrLn "\nCombinations for teams of 2:"
    let pizzaCombs = elems $ powerSet pizzas
    let pizzaComb2s = filter ((== 2) . length) pizzaCombs
    let ingredientsPerPizzaComb2 = map totalIngredients pizzaComb2s
    pPrint pizzaComb2s
    putStrLn "\nIngredients per pizza combination of 2:"
    pPrint ingredientsPerPizzaComb2
