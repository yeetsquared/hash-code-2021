import itertools
import pprint
pp = pprint.PrettyPrinter(indent=4)

full_input = [line.split(' ')[1:] for line in open("../examples/b_little_bit_of_everything.in").read().split("\n")]


numberTeam = list(map(int,full_input[0][:3]))
pizzas = full_input[1:-1]

def team_combs(numberTeam, nbPizzas):
    print(nbPizzas)
    string_stream = [2]*numberTeam[0]+[3]*numberTeam[1]+[4]*numberTeam[2]

    all_comb = []
    for i in range(1,sum(numberTeam)):
        for comb in itertools.combinations(string_stream,i):
            if not sum(map(int,comb))>nbPizzas:
                all_comb.append(list(comb))
    
    # clean a bit
    already = []
    result = []
    for i in all_comb:
        if i not in already:
            result.append(i)
            already.append(i)

    return result

# pp.pprint(team_combs(numberTeam, len(pizzas)))

def pizza_comb(teamComb, pizzas):
    """
    pizza_combinations [
        combination : [
            teams : [
                pizzas : [

                ]
            ]
        ]
    ]
    """
    pComb = []
    nb = sum(teamComb)
    for comb in itertools.permutations(pizzas, nb):
        index = 0
        comb_spl = []
        for i in teamComb:
            comb_spl.append(list(comb[index:index+i]))
            index+=i
        pComb.append(comb_spl)
    return(pComb)

# pp.pprint(pizza_comb([2,2], pizzas))

def all_pizza_comb(teamCombs, pizzas):
    return [pizza_comb(teamComb, pizzas) for teamComb in teamCombs]

# pp.pprint(all_pizza_comb(team_combs(numberTeam,len(pizzas))))

def ingredient_score(team_pizza):
    already = []
    for pizza in team_pizza:
        for ingredient in pizza:
            if ingredient not in already:
                already.append(ingredient)
    return len(already)**2

def score_comb(comb):
    score = 0
    team = []
    for team_pizza in comb:
        team.append(len(team_pizza))
        score+=ingredient_score(team_pizza)
    return {
        "comb":comb,
        "score":score
    }

def pizza_index(pizza):
    return pizzas.index(pizza)

def all_score(combs):
    result = []
    for main_comb in combs:
        for comb in main_comb:
            result.append(score_comb(comb))
    return result

def best_score(scores):
    cand_score = -1
    cand_ = {}
    for cand in scores:
        if cand["score"]>cand_score:
            cand_score=cand["score"]
            cand_ = cand
    return cand_

def cooler_code(cand):
    score = cand["score"]
    comb = cand["comb"]
    teams = []
    pizzas = []
    for team_pizza in comb:
        teams.append(len(team_pizza))
        for pizza in team_pizza:
            pizzas.append(pizza_index(pizza))
    return {
        "score":score,
        "teams":teams,
        "pizzas":pizzas
    }


pp.pprint(cooler_code(best_score(all_score(all_pizza_comb(team_combs(numberTeam,len(pizzas)),pizzas)))))